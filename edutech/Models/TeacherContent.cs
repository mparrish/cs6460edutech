﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace edutech.Models {
		public class TeacherContent {
				[BsonId]
				public ObjectId TeacherContentId { get; set; }
				public string Title { get; set; }
				public string Content { get; set; }
				public DateTime DateTime { get; set; }
				public bool IsPublished { get; set; }
				public string Author { get; set; }
		}
}