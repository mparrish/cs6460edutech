﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace edutech.Models {
		public class User {
				[BsonId]
				public ObjectId UserId { get; set; }
				public string Email { get; set; }
				public string Password { get; set; }
		}
}