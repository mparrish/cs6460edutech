﻿using edutech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace edutech.Controllers {
		public class UserProfileController : Controller {
				//
				// GET: /UserProfile/
				public ActionResult Index(string userId) {
						User user = null;
						if (String.IsNullOrEmpty(userId)) {
								user = lib.GetUserByEmail(HttpContext.Session["cs6460"] as string);
						} else {
								user = lib.GetUserById(userId);
						}

						var teacherContent = new List<TeacherContent>();
						if (user != null) {
								teacherContent = lib.GetTeacherContentList(user);
								var a = 0;
						}
						
						

						return View("Index", teacherContent);
				}
		}
}