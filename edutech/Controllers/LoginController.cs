﻿using edutech.Models;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace edutech.Controllers {
		public class LoginController : Controller {
				//
				// GET: /Login/
				public ActionResult Index() {
						return View("index", lib.GetSharedLayout(HttpContext, ViewData));
				}

				public ActionResult LoginUser(string email, string password) {
						if (String.IsNullOrEmpty(email) || String.IsNullOrEmpty(password)) {
								throw new Exception("Required fields are not set for login.");
						}

						var user = lib.GetUserByEmail(email);
						if (user != null) {
								if (user.Email == email && user.Password == password) {
										//var cookie = new HttpCookie("cs6460");
										//cookie.Value = user[0].Email;
										//this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
										//TempData["isLoggedIn"] = user[0].Email;

										HttpContext.Session.Add("cs6460", user.Email);

										return RedirectToAction("Index", "Home");
								}
						}
						return RedirectToAction("Index");
				}

				public ActionResult LogoutUser() {
						//this.ControllerContext.HttpContext.Response.Cookies.Remove("cs6460");
						//TempData["isLoggedIn"] = null;
						HttpContext.Session.Remove("cs6460");
						return RedirectToAction("Index", "Home");
				}
		}
}