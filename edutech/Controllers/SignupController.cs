﻿using edutech.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace edutech.Controllers {
		public class SignupController : Controller {
				//
				// GET: /Signup/
				public ActionResult Index() {
						return View("index", lib.GetSharedLayout(HttpContext, ViewData));
				}

				[HttpPost]
				public ActionResult SignupUser(string email, string password, string password2) {
						if (String.IsNullOrEmpty(email) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(password2)) {
								throw new Exception("Required fields are not set.");
						}

						var user = new User() { 
								Email = email,
								Password = password
						};

						var server = MongoServer.Create("mongodb://127.0.0.1");
						var db = server.GetDatabase("cs6460");
						var users = db.GetCollection<User>("users");
						users.Insert(user);
						return RedirectToAction("Index", "Login");
				}
		}
}