﻿using edutech.Models;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace edutech.Controllers {
		public class HomeController : Controller {
				//
				// GET: /Home/
				public ActionResult Index() {
						var session = HttpContext.Session["cs6460"];

						if (session != null) {
								var email = session as string;
								var user = lib.GetUserByEmail(email);

								if (user != null && !String.IsNullOrEmpty(email)) {
										ViewData["Email"] = user.Email;
										ViewData["UserId"] = user.UserId;
										return View("index", lib.GetSharedLayout(HttpContext, ViewData));
								}
						}

						return View("index", lib.GetSharedLayout(HttpContext, ViewData));
				}
		}
}