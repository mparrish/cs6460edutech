﻿using edutech.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace edutech.Controllers {
		public class TeacherContentController : Controller {
				//
				// GET: /TeacherContent/
				public ActionResult Index() {
						var session = HttpContext.Session["cs6460"];
						if (session == null) {
								return RedirectToAction("Index", "Login");
						}

						return View("index", lib.GetSharedLayout(HttpContext, ViewData));
				}

				[HttpPost]
				public string Save(string title = "", string content = "") {
						var email = HttpContext.Session["cs6460"] as String;
						var user = lib.GetUserByEmail(email);
						if (user != null) {
								var teacherContent = new TeacherContent() { 
										Author = email,
										Content = content,
										DateTime = DateTime.Now,
										IsPublished = false,
										Title = title
								};
								lib.AddContent(teacherContent);
						}
						return "done";
				}

				public ActionResult ViewContent(string contentId) {
						var teacherContent = lib.GetTeacherContentById(contentId);
						return View("ViewContent", "_layoutPage2", teacherContent);
				}
		}
}