﻿using edutech.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace edutech {
		public class lib {
				internal static string GetSharedLayout(HttpContextBase HttpContext, ViewDataDictionary ViewData) {
						var session = HttpContext.Session["cs6460"] as String;
						if (String.IsNullOrEmpty(session)) {
								ViewData["Email"] = null;
								return "_LayoutPage1";
						}
						var email = session;
						ViewData["Email"] = email;
						return "_LayoutPage2";
				}

				internal static User GetUserByEmail(string email) {
						var server = MongoServer.Create("mongodb://127.0.0.1");
						var db = server.GetDatabase("cs6460");
						var users = db.GetCollection<User>("users");
						var userResults = users.Find(Query.EQ("Email", email));
						var user = userResults.ToList<User>();
						if (user.Count == 1) {
								return user[0];
						}
						return null;
				}

				internal static User GetUserById(string userId) {
						var server = MongoServer.Create("mongodb://127.0.0.1");
						var db = server.GetDatabase("cs6460");
						var users = db.GetCollection<User>("users");
						var user = users.FindOne(Query.EQ("_id", ObjectId.Parse(userId))) as User;
						return user;
				}

				internal static void AddContent(TeacherContent teacherContent) {
						var server = MongoServer.Create("mongodb://127.0.0.1");
						var db = server.GetDatabase("cs6460");
						var collection = db.GetCollection<TeacherContent>("teachercontent");
						collection.Insert<TeacherContent>(teacherContent);
				}

				internal static List<TeacherContent> GetTeacherContentList(User user) {
						List<TeacherContent> teacherContent = new List<TeacherContent>();
						var server = MongoServer.Create("mongodb://127.0.0.1");
						var db = server.GetDatabase("cs6460");
						var collection = db.GetCollection<TeacherContent>("teachercontent");

						teacherContent = collection.Find(Query.EQ("Author", user.Email)).ToList<TeacherContent>();
						return teacherContent;
				}

				internal static TeacherContent GetTeacherContentById(string teacherContentId) {
						var server = MongoServer.Create("mongodb://127.0.0.1");
						var db = server.GetDatabase("cs6460");
						var collection = db.GetCollection<TeacherContent>("teachercontent");

						var teacherContent = collection.FindOne(Query.EQ("_id", ObjectId.Parse(teacherContentId))) as TeacherContent;
						return teacherContent;
				}
		}
}